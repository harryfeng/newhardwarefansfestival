下载网页后用服务器直接运行：/innohuborigin/source/src/index.html

有两个index，记得是src里面的index

网页首先运行：

/innohuborigin/source/src/js/app.js

然后看这行

 $urlRouterProvider
            .otherwise('/app/dashboard-v1');

然后看这个：

.state('app.dashboard-v1', {
                url: '/dashboard-v1',
                templateUrl: 'tpl/app_dashboard_v1.html'
            })
            
            然后主页就出现这个页面

1: Please put the files on a server or local host to preview. 
eg: put the "/src" files under a localhost "src" folder

looks like:
"src/css"
"src/fonts"
"src/img"
"src/js"
"src/tpl"
"src/l10n"
"src/index.html"

then preview:  http://localhost/src/index.html  in your browser.

2: Documents locate "tpl/docs.html" or "http://localhost/src/index.html#/app/docs"
online: http://flatfull.com/themes/angulr/#/app/docs